Kalah Game Api implementation
==============

- checking out

  clone or download the project from 

         https://bitbucket.org/amilabanuka/kalah-api

- Build

         mvn package
         
- Run using Spring Boot Maven plugin

         mvn spring-boot:run
         
- Run using standalone JAR

         java -jar target/backbase.test.kalah-0.0.1-SNAPSHOT

- Invoke Game web services

        curl http://localhost:8080/api/kalah
        
        curl http://localhost:8080/api/kalah/{gameId}/play/{position}
        
The default configuration is to have 6 Pits per side and 6 stones per pit. 
This can be configured from GameService.java class constants
 
        NUMBER_OF_HOUSES - number of pits per side
        NUMBER_OF_STONES - number of stones per pit
    
- Creating a New game

        curl http://localhost:8080/api/kalah

This returns a new Game instance with starting player and pits map.

```json
{
     "gameId": "92d0e517-b2f0-4663-9ebc-af3944d1b11d",
     "gameBoard": {
         "board": [ 6, 6, 6, 6, 6, 6, 0, 6, 6, 6, 6, 6, 6, 0 ],
         "player1Store": 6,
         "player2Store": 13
     },
     "currentPlayer": 1
 }
```

The indexes are zero based.

- playing the game

     `  curl http://localhost:8080/api/kalah/{gameId}/play/{position}`

The response will be either validation error of the input, game end result or 
the new state of the game

  **Validation Error Response**
  
  If there is an error in the request, message similar to below will be returned. 
  Important field is success which is set to false.
  
```json
{
    "success": false,
    "gameOver": false,
    "winPlayer": 0,
    "message": "Invalid Player",
    "currentGame": null
}
```

  **Game Over**
  
If the game is over, a message simmilar to this below will be returned. 
Important fields are gameOver whcih is set to true and the winPlayer to indicate who win
  
```json
{
    "success": true,
    "gameOver": true,
    "winPlayer": 1,
    "message": "Player 1 win [56] vs [16]",
    "currentGame": null
}
```
  
**Next state of the board**
  
Indicates the next state of the board after a successful move and the next player

```json
{
    "success": true,
    "gameOver": false,
    "winPlayer": 0,
    "message": "Turn end. it is now turn of [2]",
    "currentGame": {
        "gameId": null,
        "gameBoard": {
            "board": [ 6, 0, 7, 7, 7, 7, 1, 7, 6, 6, 6, 6, 6, 0 ],
            "player1Store": 6,
            "player2Store": 13
        },
        "currentPlayer": 2
    }
}

```

important fields are the current game which indicates the new state of board and currentPlayer which indicates the next player who has to play