package org.banuka.backbase.test;

import org.banuka.backbase.test.config.AppConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Initialize the spring boot application
 */
@SpringBootApplication
public class Application {

  public static void main(String[] args) {
    SpringApplication.run(AppConfig.class, args);
  }
}
