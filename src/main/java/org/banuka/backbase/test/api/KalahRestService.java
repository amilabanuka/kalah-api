package org.banuka.backbase.test.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.banuka.backbase.test.model.api.Game;
import org.banuka.backbase.test.model.api.GameResult;
import org.banuka.backbase.test.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Rest APi endpoint to play the game
 */
@Component
@Path("/kalah")
public class KalahRestService {

  @Autowired
  private GameService gameService;

  /**
   * Initialize a new game
   *
   * @return returns the API representation of the game
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Game getNewGame() {
    return gameService.startNewGame();

  }

  /**
   * play the game
   *
   * @param gameId : id of the game to identofy which game is being played
   * @param position: position of the house the user is playing
   * @return: api representation of the result of the requested move
   */
  @GET
  @Path("game/{gameId}/play/{position}")
  @Produces(MediaType.APPLICATION_JSON)
  public GameResult playGame(@PathParam("gameId") String gameId,
      @PathParam("position") String position) {
    try {
      Integer positionNumber = Integer.parseInt(position);
      return gameService.playGame(gameId, positionNumber);
    } catch (NumberFormatException e) {
      return GameResult.FailureResponse("Game play position has to be a number");
    }

  }

}
