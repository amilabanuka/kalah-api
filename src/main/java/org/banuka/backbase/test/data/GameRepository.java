package org.banuka.backbase.test.data;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.banuka.backbase.test.model.service.GameModel;
import org.springframework.stereotype.Service;

/**
 * creates and holds game in memory store if a horizontal scalling is needed, this can changes to a
 * database repository so that another node can serve the request and can withstand server failure
 */
@Service
public class GameRepository {

  /**
   * In memory data structure to hold different games
   */
  private Map<String, GameModel> gameStore = new HashMap<>();

  /**
   * search a game by its id
   *
   * @param gameId : id of the game
   * @return model of the game from game store
   */
  public GameModel findGameById(String gameId) {
    return gameStore.get(gameId);
  }

  /**
   * creates new new game and store it in the game store
   *
   * @param gameModel: mode of the game to be creted
   * @return: id of the game created
   */
  public String createGameModel(GameModel gameModel) {
    String gameId = UUID
        .randomUUID()
        .toString();
    gameStore.put(gameId, gameModel);
    return gameId;
  }
}
