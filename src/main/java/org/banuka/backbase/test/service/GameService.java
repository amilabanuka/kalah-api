package org.banuka.backbase.test.service;


import org.banuka.backbase.test.data.GameRepository;
import org.banuka.backbase.test.model.api.Game;
import org.banuka.backbase.test.model.api.GameResult;
import org.banuka.backbase.test.model.service.CircularIterator;
import org.banuka.backbase.test.model.service.GameModel;
import org.banuka.backbase.test.model.service.GamePlayException;
import org.banuka.backbase.test.model.service.House;
import org.banuka.backbase.test.model.service.Pit;
import org.banuka.backbase.test.model.service.Player;
import org.banuka.backbase.test.model.service.Store;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * GameService class initiate the game and runs the game. Gmae logic is stored here.
 * The game can be configured by changing the two parameters NUMBER_OF_HOUSES and NUMBER_OF_STONES
 * accordingly.
 */
@Service
public class GameService {

  private static final Logger logger = LoggerFactory.getLogger(GameService.class);

  private static final int NUMBER_OF_HOUSES = 6;
  private static final int NUMBER_OF_STONES = 6;


  @Autowired private GameRepository gameRepository;

  /**
   * Starts a new game. call the game reporitory and creates the game and returns the game board
   * data and the id of the game for playing the game.
   *
   * @return Returns game date including Game board, Game id and the stones count for each pit.
   */
  public Game startNewGame() {
    logger.info("Creating a new Game");
    GameModel gameModel = new GameModel(NUMBER_OF_HOUSES, NUMBER_OF_STONES);
    logger.debug("Calling game repository");
    String gameId = gameRepository.createGameModel(gameModel);
    logger.debug("Game created with id [" + gameId + "]");
    logger.info("Returning created game");
    return new Game(gameId, gameModel);

  }

  /**
   * when called with id of the game and the position the user intended to play,
   * 1) validates the input and game data
   * 2) play the game
   * 3) returns the validation error, new game board status, win/draw status accordingly
   *
   * @param gameId Id of the game to link to correct game.
   * @param position position of the map to play.
   * @return alidation error, or new game board status, or win/draw status accordingly
   */
  public GameResult playGame(String gameId, int position) {
    logger.info("Playing game [" + gameId + "] on position [" + position + "]");
    GameModel game;
    try {
      game = validateGameConditionsAndRetrieveGame(gameId, position);
    } catch (GamePlayException e) {
      return GameResult.FailureResponse(e.getMessage());
    }
    return play(game, position);
  }

  private GameResult play(GameModel gameModel, int positions) {
    logger.debug("Successfully validated. playing the game");
    CircularIterator<Pit> gameBoardIterator = gameModel.getPitsIterator(positions);
    Pit playingPit = gameModel.getPits().get(positions);
    int stones = playingPit.takeStones();
    logger.debug("Cleared house [" + positions + "}");
    for (int remainingStonesInHand = stones; remainingStonesInHand > 0; remainingStonesInHand--) {
      Pit pit = gameBoardIterator.next();
      if (pit instanceof Store && pit.getPlayer() != gameModel.getCurrentPlayer()) {
        logger.debug("Other player Store found. Skipping");
        pit = gameBoardIterator.next();
      }
      if (pit instanceof House && remainingStonesInHand == 1 && pit.isEmpty()
          && pit.getPlayer() == gameModel.getCurrentPlayer()
          && !((House) pit).getOppositeHouse().isEmpty()) {
        logger.debug("Landed on empty pit with opposite pit not empty. capturing the stones.");
        gameModel.getCurrentPlayer().getStore().addStone();
        gameModel.getCurrentPlayer().getStore()
            .addStones(((House) pit).getOppositeHouse().takeStones());

      } else {
        pit.addStone();
      }


    }
    if (isGameOver(gameModel)) {
      logger.debug("Game is over. calculating the winner");
      return determineWin(gameModel);
    }

    return getMoveResultWithTurnEndDecesion(gameModel, gameBoardIterator);
  }

  private GameResult getMoveResultWithTurnEndDecesion(GameModel gameModel,
      CircularIterator<Pit> gameBoardIterator) {
    if (gameBoardIterator.getCurrentElement() == gameModel.getCurrentPlayer().getStore()) {
      return GameResult.successResponse("You get another turn", gameModel);
    } else {
      gameModel.turnOver();
      return GameResult.successResponse(
          "Turn end. it is now turn of [" + gameModel.getCurrentPlayer().getId() + "]",
          gameModel);

    }
  }

  private boolean isGameOver(GameModel gameModel) {
    int player1RemainingStones = gameModel.getPlayer1().numberOfStonesInHouses();
    int player2RemainingStones = gameModel.getPlayer2().numberOfStonesInHouses();
    if (player1RemainingStones == 0) {
      gameModel.setGameOver(true);
      clearBoard(gameModel.getPlayer2());
      return true;
    } else if (player2RemainingStones == 0) {
      gameModel.setGameOver(true);
      clearBoard(gameModel.getPlayer1());
      return true;
    }
    return false;
  }

  private GameResult determineWin(GameModel gameModel) {
    String message;
    int winPlayer;
    if (gameModel.getPlayer1().getStore().getStones() > gameModel.getPlayer2().getStore()
        .getStones()) {
      logger.info(
          "Player 1 win [" + gameModel.getPlayer1().getStore().getStones() + "] vs [" + gameModel
              .getPlayer2().getStore()
              .getStones() + "]");
      message = "Player 1 win";
      winPlayer = 1;
    } else if (gameModel.getPlayer2().getStore().getStones() > gameModel.getPlayer1().getStore()
        .getStones()) {
      logger.info(
          "Player 2 win [" + gameModel.getPlayer1().getStore().getStones() + "] vs [" + gameModel
              .getPlayer2().getStore()
              .getStones() + "]");
      message = "Player 2 win";
      winPlayer = 2;
    } else {
      logger.info(
          "Game is draw [" + gameModel.getPlayer1().getStore().getStones() + "] vs [" + gameModel
              .getPlayer2().getStore()
              .getStones() + "]");
      message = "Game Draw";
      winPlayer = 0;
    }
    return GameResult.winResponse(winPlayer, message);


  }

  private void clearBoard(Player player) {
    player.getStore().addStones(player.numberOfStonesInHouses());
    player.getHouses().forEach(house -> house.takeStones());
  }


  private GameModel validateGameConditionsAndRetrieveGame(String gameId, int position)
      throws GamePlayException {
    logger.debug("Searching game [" + gameId + "]");
    GameModel game = gameRepository.findGameById(gameId);
    if (game == null) {
      logger.info("A Game with id [" + gameId + "] mot found");
      throw new GamePlayException("Invalid Game Id");
    }
    logger.debug("Game with [" + gameId + "] found");
    if (game.isGameOver()) {
      logger.error("the game is over");
      throw new GamePlayException("The game is over");
    }
    if (position >= game.getPits().size()) {
      logger.info("Requested position [" + position + "] is out of the  game board");
      throw new GamePlayException("Playing index is not correct");
    }
    if (position == game.getPits().indexOf(game.getPlayer1().getStore()) || position == game
        .getPits()
        .indexOf(game.getPlayer2().getStore())) {
      logger.info("Requested position [" + position + "]is a scoring well");
      throw new GamePlayException("Cannot play on a scoring well");
    }
    if (game.getCurrentPlayer() != game.getPits().get(position).getPlayer()) {
      logger.info(
          "Current player is [" + game.getCurrentPlayer().getId() + "] but trying to play ["
              + position
              + "] which is of player [" + game.getPits().get(position).getPlayer().getId() + "]");
      throw new GamePlayException("Invalid Player");
    }
    if (game.getPits().get(position).getStones() == 0) {
      logger.info("There are no stones in the selected pit [" + position + "]");
      throw new GamePlayException("Invalid position");
    }

    return game;
  }
}
