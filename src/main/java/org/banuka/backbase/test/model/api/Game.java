package org.banuka.backbase.test.model.api;

import org.banuka.backbase.test.model.service.GameModel;

/**
 * API representation of the current game including the board state, game id and the current player
 */
public class Game {

  private String gameId;

  private GameBoard gameBoard;

  private int currentPlayer;

  public Game(String gameId, GameModel gameModel) {
    this.gameId = gameId;
    this.currentPlayer = gameModel.getCurrentPlayer().getId();
    this.gameBoard = new GameBoard(gameModel);
  }

  public Game(GameModel gameModel) {
    this.gameBoard = new GameBoard(gameModel);
    this.currentPlayer = gameModel.getCurrentPlayer().getId();
  }

  public String getGameId() {
    return gameId;
  }

  public void setGameId(String gameId) {
    this.gameId = gameId;
  }

  public GameBoard getGameBoard() {
    return gameBoard;
  }

  public void setGameBoard(GameBoard gameBoard) {
    this.gameBoard = gameBoard;
  }

  public int getCurrentPlayer() {
    return currentPlayer;
  }

  public void setCurrentPlayer(int currentPlayer) {
    this.currentPlayer = currentPlayer;
  }
}
