package org.banuka.backbase.test.model.service;

import java.util.ArrayList;
import java.util.List;

/**
 * Holds the status of the game. current player, number of stones in each pit and overall game
 * status
 */
public class GameModel {

  private Player currentPlayer;

  private Player player1;

  private Player player2;

  private boolean gameOver;

  private List<Pit> pits = new ArrayList<>();

  private int numberOfHouses;
  private int numberOfStones;


  public GameModel(int numberOfHouses, int numberOfStones) {
    this.numberOfHouses = numberOfHouses;
    this.numberOfStones = numberOfStones;
    initGame();
  }

  /**
   * initialize the game at the start by creating the players and pits
   */
  private void initGame() {
    player1 = new Player(1);
    for (int i = 0; i < numberOfHouses; i++) {
      House house = new House(numberOfStones, player1);
      pits.add(house);
      player1.getHouses().add(house);
    }
    Store store1 = new Store(0, player1);
    pits.add(store1);
    player1.setStore(store1);

    player2 = new Player(2);
    for (int i = 0; i < numberOfHouses; i++) {
      House house = new House(numberOfStones, player2);
      pits.add(house);
      House oppositeHouse = player1.getHouses().get(numberOfHouses - i - 1);
      house.setOppositeHouse(oppositeHouse);
      oppositeHouse.setOppositeHouse(house);
      player2.getHouses().add(house);
    }
    Store store2 = new Store(0, player2);
    pits.add(store2);
    player2.setStore(store2);
    currentPlayer = player1;
  }

  /**
   * at the end of the turn, switches the current player
   */
  public void turnOver() {
    if (currentPlayer == player1) {
      currentPlayer = player2;
    } else {
      currentPlayer = player1;
    }
  }


  public Player getCurrentPlayer() {
    return currentPlayer;
  }

  public void setCurrentPlayer(Player currentPlayer) {
    this.currentPlayer = currentPlayer;
  }

  public CircularIterator<Pit> getPitsIterator(int position) {
    return new CircularIterator<>(pits, position);
  }

  public List<Pit> getPits() {
    return pits;
  }

  public Player getPlayer1() {
    return player1;
  }

  public Player getPlayer2() {
    return player2;
  }

  public boolean isGameOver() {
    return gameOver;
  }

  public void setGameOver(boolean gameOver) {
    this.gameOver = gameOver;
  }
}
