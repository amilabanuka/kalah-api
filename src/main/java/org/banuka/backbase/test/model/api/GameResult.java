package org.banuka.backbase.test.model.api;

import org.banuka.backbase.test.model.service.GameModel;

/**
 * API response for the game play. returns the current state of the game after completing the
 * request. it can be validation error, new state or game end result
 */
public class GameResult {

  private boolean success;

  private boolean gameOver;

  private int winPlayer;

  private String message;

  private Game currentGame;

  public static GameResult FailureResponse(String message) {
    GameResult gameResult = new GameResult();
    gameResult.setSuccess(false);
    gameResult.setMessage(message);
    return gameResult;
  }

  public static GameResult winResponse(int winPlayer, String message) {
    GameResult gameResult = new GameResult();
    gameResult.setSuccess(true);
    gameResult.setGameOver(true);
    gameResult.setWinPlayer(winPlayer);
    gameResult.setMessage(message);
    return gameResult;
  }

  public static GameResult successResponse(String message, GameModel gameModel) {
    GameResult gameResult = new GameResult();
    gameResult.setSuccess(true);
    gameResult.setMessage(message);
    gameResult.setCurrentGame(new Game(gameModel));

    return gameResult;
  }

  public boolean isSuccess() {
    return success;
  }

  public void setSuccess(boolean success) {
    this.success = success;
  }

  public boolean isGameOver() {
    return gameOver;
  }

  public void setGameOver(boolean gameOver) {
    this.gameOver = gameOver;
  }

  public int getWinPlayer() {
    return winPlayer;
  }

  public void setWinPlayer(int winPlayer) {
    this.winPlayer = winPlayer;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Game getCurrentGame() {
    return currentGame;
  }

  public void setCurrentGame(Game currentGame) {
    this.currentGame = currentGame;
  }
}
