package org.banuka.backbase.test.model.service;

/**
 * Represents the stores of each players where they keep captured stones
 */
public class Store extends Pit {

  public Store(int stones, Player player) {
    super(stones, player);
  }
}
