package org.banuka.backbase.test.model.service;

/**
 * is thrown if a validation of the request failed.
 */
public class GamePlayException extends Exception {

  private String message;

  public GamePlayException(String message) {
    this.message = message;

  }

  @Override
  public String getMessage() {
    return message;
  }
}
