package org.banuka.backbase.test.model.service;

import java.util.List;
import java.util.ListIterator;

/**
 * Extension of a the iterator which iterate in circular manner.
 *
 * @param <T> : Defines what type of the List
 */
public class CircularIterator<T> {

  private List<T> list;
  private int currentIndex;

  private ListIterator<T> listIterator;
  private T currentElement;

  /**
   * intialize the iterator from and move to the starting point
   *
   * @param list : list to iterator circular way
   * @param index : starting index
   */
  public CircularIterator(List<T> list, int index) {
    this.list = list;
    this.listIterator = list.listIterator(index);
    this.currentElement = next();
  }

  /**
   * Returns the element at the current iterator position
   *
   * @return element at the current iterator position
   */
  public T getCurrentElement() {
    return currentElement;
  }

  /**
   * move the iterator to the next in the list. if the list is over move the iterator to start
   * position
   */
  public T next() {
    if (listIterator.hasNext()) {
      currentIndex = listIterator.nextIndex();
      currentElement = listIterator.next();
      return currentElement;
    }
    listIterator = list.listIterator();
    return next();
  }

}
