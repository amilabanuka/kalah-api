package org.banuka.backbase.test.model.service;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents each of the two players of the game and their houses and store.
 */
public class Player {

  private int id;
  private List<House> houses = new ArrayList<>();
  private Store store;

  public Player(int id) {
    this.id = id;
  }

  public int numberOfStonesInHouses() {
    return houses.stream().mapToInt(pit -> pit.getStones()).sum();
  }

  public int getId() {
    return id;
  }

  public List<House> getHouses() {
    return houses;
  }

  public Store getStore() {
    return store;
  }

  public void setStore(Store store) {
    this.store = store;
  }
}
