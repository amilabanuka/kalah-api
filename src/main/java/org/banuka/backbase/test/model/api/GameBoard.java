package org.banuka.backbase.test.model.api;

import java.util.ArrayList;
import java.util.List;
import org.banuka.backbase.test.model.service.GameModel;

/**
 * API representation of the current state of the board
 */
public class GameBoard {

  private List<Integer> board;
  private int player1Store;
  private int player2Store;

  public GameBoard(GameModel gameModel) {
    this.board = new ArrayList<>();
    gameModel.getPits().forEach(pit -> board.add(pit.getStones()));
    player1Store = gameModel.getPits().indexOf(gameModel.getPlayer1().getStore());
    player2Store = gameModel.getPits().indexOf(gameModel.getPlayer2().getStore());
  }

  public List<Integer> getBoard() {
    return board;
  }

  public void setBoard(List<Integer> board) {
    this.board = board;
  }

  public int getPlayer1Store() {
    return player1Store;
  }

  public int getPlayer2Store() {
    return player2Store;
  }
}
