package org.banuka.backbase.test.model.service;

/**
 * House is a pit owned by a player
 */
public class House extends Pit {

  private House oppositeHouse;

  public House(int stones, Player player) {
    super(stones, player);
  }

  public House getOppositeHouse() {
    return oppositeHouse;
  }

  public void setOppositeHouse(House oppositeHouse) {
    this.oppositeHouse = oppositeHouse;
  }
}
