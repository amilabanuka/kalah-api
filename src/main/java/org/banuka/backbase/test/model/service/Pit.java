package org.banuka.backbase.test.model.service;

/**
 * general representation of a position at game board. this has to be either a House or a Store
 */
public abstract class Pit {

  private int stones;
  private Player player;

  public Pit(int stones, Player player) {
    this.stones = stones;
    this.player = player;
  }

  public int takeStones() {
    int currentStones = stones;
    stones = 0;
    return currentStones;
  }

  public boolean isEmpty() {
    return stones == 0;
  }

  public void addStone() {
    stones++;
  }

  public void addStones(int newStones) {
    stones += newStones;
  }

  public int getStones() {
    return stones;
  }

  public Player getPlayer() {
    return player;
  }
}
