package org.banuka.backbase.test.service;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.AssertJUnit.assertEquals;

import java.util.UUID;
import org.banuka.backbase.test.data.GameRepository;
import org.banuka.backbase.test.model.api.Game;
import org.banuka.backbase.test.model.api.GameResult;
import org.banuka.backbase.test.model.service.GameModel;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by amila on 11/10/2017.
 */
@Test
public class GameServiceTest {

  private GameService service;

  @Mock private GameRepository gameRepository;

  @BeforeClass
  public void init() {
    service = new GameService();
    gameRepository = Mockito.mock(GameRepository.class);
    ReflectionTestUtils.setField(service, "gameRepository", gameRepository);
  }

  @Test
  public void shouldInitialiseNewGameBoardAndSaveItToStoreWhenNewGameCalled() {
    String uuid = UUID
        .randomUUID()
        .toString();
    when(gameRepository.createGameModel(any(GameModel.class))).thenReturn(uuid);
    Game game = service.startNewGame();
    assertNotNull(game, "The Game board response should not be null");
    assertEquals("Game should start with first player", 1, game.getCurrentPlayer());
    assertEquals("Shpould Return the same Game Id Repo returned", uuid, game.getGameId());
    for (int i = 0; i < 14; i++) {
      if (i == 6 || i == 13) {
        assertEquals("Player Scoring well should be zero", 0, game
            .getGameBoard()
            .getBoard()
            .get(i)
            .intValue());
      } else {
        assertEquals("Player positions should be 6", 6, game
            .getGameBoard()
            .getBoard()
            .get(i)
            .intValue());
      }
    }
    verify(gameRepository).createGameModel(any(GameModel.class));
    verifyNoMoreInteractions(gameRepository);

  }

  @Test
  public void shouldReturnFailIfTheGameIdIsNotFound() {
    String gameId = UUID
        .randomUUID()
        .toString();

    when(gameRepository.findGameById(gameId)).thenReturn(null);
    GameResult gameResult = service.playGame(gameId, 5);
    assertNotNull(gameResult, "Game result Should not be null");
    assertFalse(gameResult.isSuccess(), "Game should fail");
    verify(gameRepository).findGameById(gameId);
    verifyNoMoreInteractions(gameRepository);

  }

  @Test
  public void shouldReturnFailIfTheGameIdIsValidButOver() {
    String gameId = UUID
        .randomUUID()
        .toString();
    GameModel gameModel = getGameModel();
    gameModel.setGameOver(true);
    when(gameRepository.findGameById(gameId)).thenReturn(gameModel);
    GameResult gameResult = service.playGame(gameId, 15);
    assertNotNull(gameResult, "Game result Should not be null");
    assertFalse(gameResult.isSuccess(), "Game should fail");
    verify(gameRepository).findGameById(gameId);
    verifyNoMoreInteractions(gameRepository);

  }

  @Test
  public void shouldReturnFailIfTheGameIdIsValidButPlayingPositionOutOfRange() {
    String gameId = UUID
        .randomUUID()
        .toString();
    GameModel gameModel = getGameModel();
    when(gameRepository.findGameById(gameId)).thenReturn(gameModel);
    GameResult gameResult = service.playGame(gameId, 15);
    assertNotNull(gameResult, "Game result Should not be null");
    assertFalse(gameResult.isSuccess(), "Game should fail");
    verify(gameRepository).findGameById(gameId);
    verifyNoMoreInteractions(gameRepository);

  }


  @Test
  public void shouldReturnFailIfTheGameIdIsValidButPlayingPositionIsPlayer2ScoringWell() {
    String gameId = UUID
        .randomUUID()
        .toString();
    GameModel gameModel = getGameModel();
    when(gameRepository.findGameById(gameId)).thenReturn(gameModel);
    GameResult gameResult = service.playGame(gameId, 13);
    assertNotNull(gameResult, "Game result Should not be null");
    assertFalse(gameResult.isSuccess(), "Game should fail");
    verify(gameRepository).findGameById(gameId);
    verifyNoMoreInteractions(gameRepository);

  }


  @Test
  public void shouldReturnFailIfTheGameIdIsValidButPlayer1triesToPlayPlayer2() {
    String gameId = UUID
        .randomUUID()
        .toString();
    GameModel gameModel = getGameModel();
    when(gameRepository.findGameById(gameId)).thenReturn(gameModel);
    GameResult gameResult = service.playGame(gameId, 8);
    assertNotNull(gameResult, "Game result Should not be null");
    assertFalse(gameResult.isSuccess(), "Game should fail");
    verify(gameRepository).findGameById(gameId);
    verifyNoMoreInteractions(gameRepository);

  }


  @Test
  public void shouldReturnFailIfTheGameIdIsValidButPlayer2triesToPlayPlayer1() {
    String gameId = UUID
        .randomUUID()
        .toString();
    GameModel gameModel = getGameModel();
    gameModel.setCurrentPlayer(gameModel.getPits().get(8).getPlayer());
    when(gameRepository.findGameById(gameId)).thenReturn(gameModel);
    GameResult gameResult = service.playGame(gameId, 4);
    assertNotNull(gameResult, "Game result Should not be null");
    assertFalse(gameResult.isSuccess(), "Game should fail");
    verify(gameRepository).findGameById(gameId);
    verifyNoMoreInteractions(gameRepository);

  }

  @Test
  public void shouldReturnFailIfTheGameIdIsValidButPositionHasNoStones() {
    String gameId = UUID
        .randomUUID()
        .toString();
    GameModel gameModel = getGameModel();
    gameModel.getPits().get(4).takeStones();
    gameModel.setCurrentPlayer(gameModel.getPits().get(4).getPlayer());
    when(gameRepository.findGameById(gameId)).thenReturn(gameModel);
    GameResult gameResult = service.playGame(gameId, 4);
    assertNotNull(gameResult, "Game result Should not be null");
    assertFalse(gameResult.isSuccess(), "Game should fail");
    verify(gameRepository).findGameById(gameId);
    verifyNoMoreInteractions(gameRepository);

  }

  @Test
  public void shouldPlayTheGameAndGetTurnEnd() {
    String gameId = UUID
        .randomUUID()
        .toString();
    GameModel gameModel = getGameModel();
    gameModel.setCurrentPlayer(gameModel.getPits().get(4).getPlayer());
    when(gameRepository.findGameById(gameId)).thenReturn(gameModel);
    GameResult gameResult = service.playGame(gameId, 4);
    assertNotNull(gameResult, "Game result Should not be null");
    assertTrue(gameResult.isSuccess(), "Game should success");
    assertEquals("Turn should end", gameModel.getPlayer2(), gameModel.getCurrentPlayer());

    verify(gameRepository).findGameById(gameId);
    verifyNoMoreInteractions(gameRepository);

  }

  @Test
  public void shouldPlayTheGameAndGetTurnContinue() {
    String gameId = UUID
        .randomUUID()
        .toString();
    GameModel gameModel = getGameModel();
    gameModel.getPits().get(3).takeStones();
    gameModel.getPits().get(3).addStones(3);
    gameModel.setCurrentPlayer(gameModel.getPits().get(4).getPlayer());
    when(gameRepository.findGameById(gameId)).thenReturn(gameModel);
    GameResult gameResult = service.playGame(gameId, 3);
    assertNotNull(gameResult, "Game result Should not be null");
    assertTrue(gameResult.isSuccess(), "Game should success");
    assertEquals("Turn should not end", gameModel.getPlayer1(), gameModel.getCurrentPlayer());
    verify(gameRepository).findGameById(gameId);
    verifyNoMoreInteractions(gameRepository);

  }

  @Test
  public void shouldPlayTheGameCaptureAndGetTurnEnd() {
    String gameId = UUID
        .randomUUID()
        .toString();
    GameModel gameModel = getGameModel();
    gameModel.getPits().get(1).takeStones();
    gameModel.getPits().get(1).addStones(2);
    gameModel.getPits().get(3).takeStones();
    gameModel.setCurrentPlayer(gameModel.getPits().get(4).getPlayer());
    when(gameRepository.findGameById(gameId)).thenReturn(gameModel);
    GameResult gameResult = service.playGame(gameId, 1);
    assertNotNull(gameResult, "Game result Should not be null");
    assertTrue(gameResult.isSuccess(), "Game should success");
    assertEquals("store should be 7", 7, gameModel.getPlayer1().getStore().getStones());
    verify(gameRepository).findGameById(gameId);
    verifyNoMoreInteractions(gameRepository);

  }


  @Test
  public void shouldPlayTheGamePlayer1Win() {
    String gameId = UUID
        .randomUUID()
        .toString();
    GameModel gameModel = getGameModel();
    gameModel.getPits().get(0).takeStones();
    gameModel.getPits().get(1).takeStones();
    gameModel.getPits().get(2).takeStones();
    gameModel.getPits().get(3).takeStones();
    gameModel.getPits().get(4).takeStones();
    gameModel.getPits().get(5).takeStones();
    gameModel.getPits().get(5).addStones(2);
    gameModel.getPlayer1().getStore().addStones(530);
    gameModel.setCurrentPlayer(gameModel.getPits().get(4).getPlayer());
    when(gameRepository.findGameById(gameId)).thenReturn(gameModel);
    GameResult gameResult = service.playGame(gameId, 5);
    assertNotNull(gameResult, "Game result Should not be null");
    assertTrue(gameResult.isSuccess(), "Game should success");
    assertTrue(gameResult.isGameOver(), "Game should over");
    assertEquals("player 1 should win", gameModel.getPlayer1().getId(), gameResult.getWinPlayer());
    verify(gameRepository).findGameById(gameId);
    verifyNoMoreInteractions(gameRepository);

  }


  @Test
  public void shouldPlayTheGameAndDraw() {
    String gameId = UUID
        .randomUUID()
        .toString();
    GameModel gameModel = getGameModel();
    gameModel.getPits().get(0).takeStones();
    gameModel.getPits().get(1).takeStones();
    gameModel.getPits().get(2).takeStones();
    gameModel.getPits().get(3).takeStones();
    gameModel.getPits().get(4).takeStones();
    gameModel.getPits().get(5).takeStones();
    gameModel.getPits().get(5).addStones(2);
    gameModel.getPits().get(7).takeStones();
    gameModel.getPits().get(8).takeStones();
    gameModel.getPits().get(9).takeStones();
    gameModel.getPits().get(10).takeStones();
    gameModel.getPits().get(11).takeStones();
    gameModel.getPits().get(12).takeStones();
    gameModel.getPits().get(12).addStone();
    gameModel.getPits().get(12).addStones(2);
    gameModel.getPits().get(0).takeStones();
    gameModel.getPlayer2().getStore().addStones(118);
    gameModel.getPlayer1().getStore().addStones(121);
    gameModel.setCurrentPlayer(gameModel.getPits().get(5).getPlayer());
    when(gameRepository.findGameById(gameId)).thenReturn(gameModel);
    GameResult gameResult = service.playGame(gameId, 5);
    assertNotNull(gameResult, "Game result Should not be null");
    assertTrue(gameResult.isSuccess(), "Game should success");
    assertTrue(gameResult.isGameOver(), "Game should over");
    assertEquals("player 1 should win", 0, gameResult.getWinPlayer());
    verify(gameRepository).findGameById(gameId);
    verifyNoMoreInteractions(gameRepository);

  }

  @Test
  public void shouldPlayTheGamePlayer2Win() {
    String gameId = UUID
        .randomUUID()
        .toString();
    GameModel gameModel = getGameModel();
    gameModel.getPits().get(7).takeStones();
    gameModel.getPits().get(8).takeStones();
    gameModel.getPits().get(9).takeStones();
    gameModel.getPits().get(10).takeStones();
    gameModel.getPits().get(11).takeStones();
    gameModel.getPits().get(12).takeStones();
    gameModel.getPits().get(12).addStones(2);
    gameModel.getPlayer2().getStore().addStones(530);
    gameModel.setCurrentPlayer(gameModel.getPits().get(10).getPlayer());
    when(gameRepository.findGameById(gameId)).thenReturn(gameModel);
    GameResult gameResult = service.playGame(gameId, 12);
    assertNotNull(gameResult, "Game result Should not be null");
    assertTrue(gameResult.isSuccess(), "Game should success");
    assertTrue(gameResult.isGameOver(), "Game should over");
    assertEquals("player 1 should win", gameModel.getPlayer2().getId(), gameResult.getWinPlayer());
    verify(gameRepository).findGameById(gameId);
    verifyNoMoreInteractions(gameRepository);

  }


  private GameModel getGameModel() {
    GameModel gameModel = new GameModel(6, 6);
    return gameModel;
  }

}
